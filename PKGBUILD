# Maintainer: Fabian Bornschein <fabiscafe@archlinux.org>
# Maintainer: Jan Alexander Steffens (heftig) <heftig@archlinux.org>
# Contributor: Jan de Groot <jgc@archlinux.org>

pkgname=gnome-calendar
pkgver=46.0
pkgrel=1
pkgdesc="Simple and beautiful calendar application designed to perfectly fit the GNOME desktop"
url="https://wiki.gnome.org/Apps/Calendar"
arch=(x86_64)
license=(GPL-3.0-or-later)
depends=(
  dconf
  evolution-data-server
  gcc-libs
  geoclue
  geocode-glib-2
  glib2
  glibc
  graphene
  gsettings-desktop-schemas
  gtk4
  hicolor-icon-theme
  libadwaita
  libedataserverui4
  libgweather-4
  libical
  libsoup3
  pango
)
makedepends=(
  git
  meson
)
optdepends=('gnome-control-center: Manage online accounts')
groups=(gnome)
_commit=64f2fb7b4d1be594e67fd4f030a34e6cfe148026  # tags/46.0^0
source=("git+https://gitlab.gnome.org/GNOME/gnome-calendar.git#commit=$_commit")
b2sums=('d3151cc8523c516ce8e60e3a4b0e669950cd24dd1b3e8212dd5b5930c33567b07862aa1f79c26f81f68e8c6d31aedfafd1ab4d10896c8372c01aa0f3710b9bbb')

pkgver() {
  cd $pkgname
  git describe --tags | sed -r 's/\.([a-z])/\1/;s/([a-z])\./\1/;s/[^-]*-g/r&/;s/-/+/g'
}

prepare() {
  cd $pkgname
}

build() {
  arch-meson $pkgname build
  meson compile -C build
}

check() {
  meson test -C build --print-errorlogs
}

package() {
  meson install -C build --destdir "$pkgdir"
}

# vim:set sw=2 sts=-1 et:
